const express = require('express')

const router = express.Router();
const controller = require('../controller/hotelController');

router
    .route('/hotel')
    .get(controller.getAllHotels)
    .post(controller.createHotel);

router
    .route('/hotel/:hotel_code')
    .get(controller.getHotel)
    .put(controller.updateHotel)
    .delete(controller.deleteHotel);

module.exports = router;