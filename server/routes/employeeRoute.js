const express = require('express');

const router = express.Router();
const controller = require('../controller/employeeController');

router
    .route('/employee')
    .get(controller.getAllEmployees)
    .post(controller.createEmployee);

router
    .route('/employee/:employee_id')
    .get(controller.getEmployee)
    .put(controller.updateEmployee)
    .delete(controller.deleteEmployee);

module.exports = router;