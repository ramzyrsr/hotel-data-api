const Sequelize = require('sequelize')

const db = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    dialect: process.env.DIALECT,
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

module.exports = db;