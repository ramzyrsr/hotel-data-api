const Employee = require('../models/employeersModel');
const methods = {};

methods.getEmployee = async (req, res) => {
    try {
        const employee_id = req.params.employee_id;

        const employee = await Employee.findOne({
            where: { employee_id }
        });

        const resPayload = {
            statusCode: 200,
            statusText: 'success',
            message: 'Employee is found',
            data: employee
        };
        res
            .json(resPayload)
            .status(200);

    } catch (error) {
        res.json({
            statusCode: 400,
            statusText: 'Bad Request',
            message: error
        })
        .status(400);
    }
};

methods.getAllEmployees = async (req, res) => {
    try {
        const employees = await Employee.findAll();
        const resPayload = {
            statusCode: 200,
            statusText: 'success',
            message: 'All employees is found',
            data: employees
        };
        res
            .json(resPayload)
            .status(200);

    } catch (error) {
        res.json({
            statusCode: 400,
            statusText: 'Bad Request',
            message: error
        })
        .status(400);
    }
};

methods.createEmployee = async (req, res) => {
    try {
        const reqPayload = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            createdAt: new Date(),
            updatedAt: new Date()
        };
        const newEmployee = Employee.create(reqPayload);

        if (newEmployee) {
            res
                .json({
                    statusCode: 201,
                    statusText: 'Created',
                    message: 'New employee is created',
                    data: reqPayload
                })
                .status(201);
        };

    } catch (error) {
        res.json({
            statusCode: 400,
            statusText: 'Bad Request',
            message: error
        })
        .status(400);
    }
};

methods.updateEmployee = async (req, res) => {
    try {
        await Employee.update(req.body, {
            where: { employee_id }
        });
        res
            .json({
                statusCode: 200,
                statusText: 'success',
                message: 'Employee is updated'
            })
            .status(200);

    } catch (error) {
        res.json({
            statusCode: 400,
            statusText: 'Bad Request',
            message: error
        })
        .status(400);
    }
};

methods.deleteEmployee = async (req, res) => {
    try {
        await Employee.destroy({
            where: { employee_id }
        });
        res
            .json({
                statusCode: 204,
                statusText: 'No Content',
                message: 'Employee is deleted'
            })
            .status(204);

    } catch (error) {
        res.json({
            statusCode: 400,
            statusText: 'Bad Request',
            message: error
        })
        .status(400);
    }
};

module.exports = methods;