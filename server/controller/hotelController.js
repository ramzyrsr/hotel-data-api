const Hotel = require('../models/hotelsModel');
const methods = {};

methods.getHotel = async (req, res) => {
    try {
        const hotel_code = req.params.hotel_code;

        const hotel = await Hotel.findOne({
            where: {
                hotel_code
            }
        })

        const resPayload = {
            statusCode: 200,
            statusText: 'success',
            message: 'Hotel is found',
            data: hotel
        };
        res
            .json(resPayload)
            .status(200);

    } catch (error) {
        res.json({
            statusCode: 400,
            statusText: 'Bad Request',
            message: error
        })
        .status(400);
    }
};

methods.getAllHotels = async (req, res) => {
    try {
        const hotels = await Hotel.findAll();
        const resPayload = {
            statusCode: 200,
            statusText: 'success',
            message: 'All hotels is found',
            data: hotels,
        };
        res
            .json(resPayload)
            .status(200);

    } catch (error) {
        res.json({
            statusCode: 400,
            statusText: 'Bad Request',
            message: error
        })
        .status(400);
    }
};

methods.createHotel = async (req, res) => {
    try {
        const reqPayload = {
            hotel_code: req.body.hotel_code,
            hotel_name: req.body.hotel_name,
            location: req.body.location,
            createdAt: new Date(),
            updatedAt: new Date()
        }
        const newHotel = Hotel.create(reqPayload);

        if (newHotel) {
            res
                .json({
                    statusCode: 201,
                    statusText: 'Created',
                    message: 'New hotel is created',
                    data: reqPayload
                })
                .status(201);
        };

    } catch (error) {
        res.json({
            statusCode: 400,
            statusText: 'Bad Request',
            message: error
        })
        .status(400);
    }
};

methods.updateHotel = async (req, res) => {
    try {
        await Hotel.update(req.body, {
            where: {
                id: req.params.id
            }
        })
        res
            .json({
                statusCode: 200,
                statusText: 'success',
                message: 'Hotel is updated'
            })
            .status(200);

    } catch (error) {
        res.json({
			statusCode: 400,
			statusText: 'Bad Request',
			message: error
		})
		.status(400);
    }
};

methods.deleteHotel = async (req, res) => {
    try {
        await Hotel.destroy({
            where: {
                id: req.params.id
            }
        })
        res
            .json({
                statusCode: 204,
                statusText: 'No Content',
                message: 'Hotel is deleted'
            })
            .status(204);

    } catch (error) {
        res.json({
			statusCode: 400,
			statusText: 'Bad Request',
			message: error
		})
		.status(400);
    }
};

module.exports = methods;