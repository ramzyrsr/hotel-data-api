const Reservation = require('../models/reservationsModel');
const methods = {};

methods.getReservation = async (req, res) => {
    try {
        const res_nr = req.params.res_nr;

        const reservation = await Reservation.findOne({
            where: { res_nr }
        });

        const resPayload = {
            statusCode: 200,
            statusText: 'success',
            message: 'Reservation is found',
            data: reservation
        };
        res
            .json(resPayload)
            .status(200);

    } catch (error) {
        res.json({
            statusCode: 400,
            statusText: 'Bad Request',
            message: error
        })
        .status(400);
    }
};

methods.getAllReservations = async (req, res) => {
    try {
        const reservations = await Reservation.findAll();
        const resPayload = {
            statusCode: 200,
            statusText: 'success',
            message: 'All reservation is found',
            data: reservations
        };
        res
            .json(resPayload)
            .status(200);

    } catch (error) {
        res.json({
            statusCode: 400,
            statusText: 'Bad Request',
            message: error
        })
        .status(400);
    }
};

methods.createReservation = async (req, res) => {
    try {
        const reqPayload = {
            hotel_code = req.body.hotel_code,
            room_id = req.body.room_id,
            guest_id = req.body.guest_id
        };
        const newReservation = Reservation.create(reqPayload);

        if (newReservation) {
            res
                .json({
                    statusCode: 201,
                    statusText: 'Created',
                    message: 'New reservation is created',
                    data: newReservation
                })
                .json(201);
        };

    } catch (error) {
        res.json({
            statusCode: 400,
            statusText: 'Bad Request',
            message: error
        })
        .status(400);
    }
};

methods.deleteReservation = async (req, res) => {
    try {
        await Reservation.destroy({
            where: { res_nr }
        });
        res
            .json({
                statusCode: 204,
                statusText: 'No Content',
                message: 'Reservation is deleted'
            })
            .status(204);

    } catch (error) {
        res.json({
            statusCode: 400,
            statusText: 'Bad Request',
            message: error
        })
        .status(400);
    }
};

module.exports = methods;