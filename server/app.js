const express = require('express');
const bp = require('body-parser');
const logger = require('morgan');
const cors = require('cors');

const app = express();

if (process.env.NODE_ENV === 'development') {
    app.use(logger('dev'));
}

app.use(cors());
app.use(bp.urlencoded({ extended: false }));
app.use(bp.json());

let hotels = require('./routes/hotelsRoute');
let employees = require('./routes/employeeRoute');

app.use('/api/v1', hotels);
app.use('/api/v1', employees);

module.exports = app;