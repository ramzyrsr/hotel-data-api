const Sequelize = require('sequelize');
const db = require('../config/db');

const Employees = db.define(
    'employees',
    {
        employee_id: {
            allowNull: false,
            autoIncrement: true,
            type: Sequelize.DataTypes.INTEGER,
            primaryKey: true
        },
        hotel_code: {
            type: Sequelize.DataTypes.STRING,
            foreignKey: true
        },
        first_name: {
            type: Sequelize.DataTypes.STRING
        },
        last_name: {
            type: Sequelize.DataTypes.STRING
        },
        phone: {
            type: Sequelize.DataTypes.STRING
        }
    },
    {
        timestamps: true
    },
    {
        classMethods: {
            associate: function(models) {
                Employees.belongsTo(models.hotel, {
                    foreignKey: 'hotel_code'
                });
            }
        }
    }
);

module.exports = Employees;