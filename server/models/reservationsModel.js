const Sequelize = require('sequelize');
const db = require('../config/db');

const Reservation = db.define(
    'reservations',
    {
        id: {
            type: Sequelize.DataTypes.INTEGER
        },
        res_nr: {
            type: Sequelize.DataTypes.INTEGER,
            primaryKey: true
        },
        hotel_code: {
            type: Sequelize.DataTypes.STRING,
            foreignKey: true
        },
        guest_id: {
            type: Sequelize.DataTypes.INTEGER,
            foreignKey: true
        },
        room_id: {
            type: Sequelize.DataTypes.INTEGER,
            foreignKey: true
        }
    },
    {
        timestamps: true
    },
    {
        classMethods: {
            associate: function(models) {
                // Reservation
            }
        }
    }
)

module.exports = Reservation;