const Sequelize = require('sequelize');
const db = require('../config/db');

const Hotel = db.define(
    'hotels',
    {
      id: {
          type: Sequelize.DataTypes.INTEGER,
      },
      hotel_code: {
        type: Sequelize.DataTypes.STRING,
        primaryKey: true,
      },
      hotel_name: {
        type: Sequelize.DataTypes.STRING,
      },
      location: {
        type: Sequelize.DataTypes.STRING,
      },
      // createdAt: {
      //   allowNull: false,
      //   type: Sequelize.DataTypes.DATE,
      //   defaultValue: new Date(),
      // },
      // updatedAt: {
      //   allowNull: false,
      //   type: Sequelize.DataTypes.DATE,
      //   defaultValue: new Date(),
      // }
    },
    {
      timestamps: true
    },
    {
        classMethods: {
            associate: function(models) {
                Hotel.hasMany(models.employees, {
                    foreignKey: 'hotel_code'
                });
            }
        }
    }
);

module.exports = Hotel;